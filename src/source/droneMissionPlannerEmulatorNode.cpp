//I/O Stream
//std::cout
#include <iostream>
#include <string>
#include <sstream>

// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"
#include "droneMissionPlannerEmulator.h"
#include <thread>
#include <curses.h>

using namespace std;


//http://www.asciitable.com/
#define ASCII_KEY_UP      65
#define ASCII_KEY_DOWN    66
#define ASCII_KEY_RIGHT   67
#define ASCII_KEY_LEFT    68


//Define step commands
#define CTE_COMMAND_YAW    0.40
#define CTE_COMMAND_PITCH  0.33
#define CTE_COMMAND_ROLL   0.33
#define CTE_COMMAND_HEIGHT 0.50


// Define controller commands define constants
#define CONTROLLER_CTE_COMMAND_SPEED        ( 1.00 )
#define CONTROLLER_STEP_COMMAND_POSITTION   ( 0.25 )
#define CONTROLLER_STEP_COMMAND_ALTITUDE    ( 0.25 )
#define CONTROLLER_STEP_COMMAND_YAW         ( 10.0 * (M_PI/180.0) )



void thread_mission_planner_command_fnc()
{
    ros::NodeHandle n;
    DroneMissionPlannerEmulator MyDroneMissionPlannerEmulator;
    MyDroneMissionPlannerEmulator.open(n);



    // ncurses initialization
//    initscr();
//    nonl();			  // NL conversions off
//    intrflush( stdscr, TRUE );	  // we want ^C, etc...
//    nodelay(stdscr, TRUE);
//    erase(); refresh();
//    (void) keypad( stdscr, TRUE );  // extended keys ON
//    curs_set( 1 );		  // make sure the cursor is visible
//    std::string s = "Command: ";


    char command = 0;
    while (ros::ok())
    {
        cin.clear();
        cin>>command;


//        wclear( stdscr );
//        wmove( stdscr, 2, 2 );
//        waddstr( stdscr, const_cast<char*>(s.c_str()) );
//        command = wgetch( stdscr );

        switch(command)
        {
            case 27:
                break;
            case 't':
                MyDroneMissionPlannerEmulator.sendCommandForTakingoff();
                //printw("Taking off"); clrtoeol();
                break;

            case 'l':
                MyDroneMissionPlannerEmulator.sendCommandForLanding();
                //printw("Landing"); clrtoeol();
                break;

            case 'h':
                MyDroneMissionPlannerEmulator.sendCommandForHovering();
                //printw("Hovering"); clrtoeol();
                break;

            case 'q':
                if(MyDroneMissionPlannerEmulator.getDroneManagerStatus().status == droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD)
                    MyDroneMissionPlannerEmulator.sendCommandInMovingManualAltitudMode(0.0, 0.0, CTE_COMMAND_HEIGHT, 0.0);
                else
                    MyDroneMissionPlannerEmulator.sendCommandInPositionControlMode(0.0, 0.0, CONTROLLER_STEP_COMMAND_ALTITUDE);
                //printw("Moving Manual Altitud"); clrtoeol();
                break;

            case 'a':
                if(MyDroneMissionPlannerEmulator.getDroneManagerStatus().status == droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD)
                    MyDroneMissionPlannerEmulator.sendCommandInMovingManualAltitudMode(0.0, 0.0, -CTE_COMMAND_HEIGHT, 0.0);
                else
                    MyDroneMissionPlannerEmulator.sendCommandInPositionControlMode(0.0, 0.0, -CONTROLLER_STEP_COMMAND_ALTITUDE);
                break;

            case 'z':
                if(MyDroneMissionPlannerEmulator.getDroneManagerStatus().status == droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD)
                    MyDroneMissionPlannerEmulator.sendCommandInMovingManualAltitudMode(0.0, 0.0, 0.0, CTE_COMMAND_YAW);
                else
                    MyDroneMissionPlannerEmulator.sendYawCommandInPositionControlMode(CONTROLLER_STEP_COMMAND_YAW);
                break;

            case 'x':
                if(MyDroneMissionPlannerEmulator.getDroneManagerStatus().status == droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD)
                    MyDroneMissionPlannerEmulator.sendCommandInMovingManualAltitudMode(0.0, 0.0, 0.0, -CTE_COMMAND_YAW);
                else
                    MyDroneMissionPlannerEmulator.sendYawCommandInPositionControlMode(-CONTROLLER_STEP_COMMAND_YAW);
                break;

            case 'r':
                if(MyDroneMissionPlannerEmulator.getDroneManagerStatus().status == droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD)
                    MyDroneMissionPlannerEmulator.sendCommandInMovingManualAltitudMode(CTE_COMMAND_PITCH, 0.0, 0.0, 0.0);
                else
                   MyDroneMissionPlannerEmulator.sendCommandInPositionControlMode(CONTROLLER_STEP_COMMAND_POSITTION, 0.0, 0.0);
                break;

            case 'f':
                if(MyDroneMissionPlannerEmulator.getDroneManagerStatus().status == droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD)
                    MyDroneMissionPlannerEmulator.sendCommandInMovingManualAltitudMode(-CTE_COMMAND_PITCH, 0.0, 0.0, 0.0);
                else
                   MyDroneMissionPlannerEmulator.sendCommandInPositionControlMode(-CONTROLLER_STEP_COMMAND_POSITTION, 0.0, 0.0);
                break;

            case 'd':
                if(MyDroneMissionPlannerEmulator.getDroneManagerStatus().status == droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD)
                    MyDroneMissionPlannerEmulator.sendCommandInMovingManualAltitudMode(0.0, -CTE_COMMAND_ROLL, 0.0, 0.0);
                else
                   MyDroneMissionPlannerEmulator.sendCommandInPositionControlMode(0.0, -CONTROLLER_STEP_COMMAND_POSITTION, 0.0);
                break;

            case 'g':
                if(MyDroneMissionPlannerEmulator.getDroneManagerStatus().status == droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD)
                    MyDroneMissionPlannerEmulator.sendCommandInMovingManualAltitudMode(0.0, CTE_COMMAND_ROLL, 0.0, 0.0);
                else
                   MyDroneMissionPlannerEmulator.sendCommandInPositionControlMode(0.0, CONTROLLER_STEP_COMMAND_POSITTION, 0.0);
                break;

            case 'j':
                MyDroneMissionPlannerEmulator.sendCommandInSpeedControlMode(CONTROLLER_CTE_COMMAND_SPEED, 0.0);
                break;

            case 'n':
                MyDroneMissionPlannerEmulator.sendCommandInSpeedControlMode(-CONTROLLER_CTE_COMMAND_SPEED, 0.0);
                break;

            case 'b':
                MyDroneMissionPlannerEmulator.sendCommandInSpeedControlMode(0.0, -CONTROLLER_CTE_COMMAND_SPEED);
                break;

            case 'm':
                MyDroneMissionPlannerEmulator.sendCommandInSpeedControlMode(0.0, CONTROLLER_CTE_COMMAND_SPEED);
                break;


            case '1':
                MyDroneMissionPlannerEmulator.sendCommandInVisualServoingMode();
                break;

            case '2':
                MyDroneMissionPlannerEmulator.sendCommandInMovingManualAltitudMode(0.0, 0.0, 0.0, 0.0);
                break;
            case '4':
            {
                droneMsgsROS::dronePositionRefCommand mission_point;
                mission_point.x = 5.0;
                mission_point.y = 5.0;
                mission_point.z = 2.5;
                MyDroneMissionPlannerEmulator.sendCommandInAbsTrajectoryControlMode(mission_point);
                break;
            }
            case '5':
                MyDroneMissionPlannerEmulator.sendCommandForLooping();
                break;
            case '6':
                MyDroneMissionPlannerEmulator.sendCommandForMovingInYaw();
                break;
            case '7':
                MyDroneMissionPlannerEmulator.sendCommandInSpeedControlMode(0.0, 0.0);
                break;
            case '8':
                MyDroneMissionPlannerEmulator.sendCommandInPositionControlMode(0.0, 0.0, 0.0);
                break;
            case '9':
            {
                std::vector<SimpleTrajectoryWaypoint> trajectory_waypoints_out;
                int initial_checkpoint_out = 0;
                bool is_periodic_out = false;
                trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint( 0.0, 0.0, 0.0) );
                trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint( 0.0, 2.0, 1.0) );
                trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint( 2.0, 0.0, 1.0) );
                trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint( 0.0, -2.0, 1.0) );
                trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint( -2.0, 0.0, 1.0) );
                MyDroneMissionPlannerEmulator.sendCommandInRelTrajectoryControlMode(&trajectory_waypoints_out,initial_checkpoint_out,is_periodic_out);
                break;
            }

        }



        ros::spinOnce();

//        clrtoeol();
//        refresh();

        //loop_rate.sleep();
    }

    //endwin();

    return;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "missionPlannerEmulator");
    ros::NodeHandle n;


    std::thread thread_mission_planner_command(thread_mission_planner_command_fnc);



    ros::spin();



    return 0;
}










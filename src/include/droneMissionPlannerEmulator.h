#ifndef DRONE_MISSION_PLANNER_EMULATOR_H
#define DRONE_MISSION_PLANNER_EMULATOR_H

// ROS
#include "ros/ros.h"

// C++ standar libraries
#include <cstdlib>
#include <stdio.h>
#include <iostream>

#include "droneModuleROS.h"

// Topic and Rosservice names
// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "droneMsgsROS/droneMissionPlannerCommand.h"
#include "droneMsgsROS/droneHLCommandAck.h"
#include "dronemoduleinterface.h"
#include "control/Controller_MidLevel_controlModes.h"
#include "droneMsgsROS/droneTrajectoryControllerControlMode.h"
#include "droneMsgsROS/setControlMode.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneStatus.h"
#include "droneMsgsROS/droneManagerStatus.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/droneAltitudeCmd.h"
#include "control/simpletrajectorywaypoint.h"
#include "communication_definition.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"



class DroneMissionPlannerEmulator : public DroneModule
{
private:
    droneMsgsROS::dronePitchRollCmd dronePitchRollCmdMsgs;
    droneMsgsROS::droneDAltitudeCmd droneDAltitudeCmdMsgs;
    droneMsgsROS::droneDYawCmd droneDYawCmdMsgs;
    droneMsgsROS::dronePositionRefCommandStamped  drone_position_reference;
    droneMsgsROS::droneSpeeds drone_speed_reference;
    droneMsgsROS::droneYawRefCommand drone_yaw_reference;
    droneMsgsROS::dronePose   current_drone_position_reference;
    droneMsgsROS::droneSpeeds current_drone_speed_reference;
    droneMsgsROS::dronePose   last_drone_estimated_GMRwrtGFF_pose;
    droneMsgsROS::droneMissionPlannerCommand msg;
    droneMsgsROS::droneStatus last_drone_status_msg;
    droneMsgsROS::droneManagerStatus last_drone_manager_status_msg;
    droneMsgsROS::droneHLCommandAck droneHLCommAckMsg;
    int32_t mp_command;
    bool tracker_is_tracking;

    ros::NodeHandle n;
    ros::Publisher mission_planer_pub;
    ros::Publisher drone_position_reference_publisher;
    ros::Publisher drone_speeds_reference_publisher;
    ros::Publisher drone_rel_trajectory_reference_publisher;
    ros::Publisher drone_abs_trajectory_reference_publisher;
    ros::Publisher dronePositionRefCommandPubl;
    ros::Publisher drone_yaw_reference_publisher;
    ros::Publisher dronePitchRollCmdPubl;
    ros::Publisher droneDAltitudeCmdPubl;
    ros::Publisher droneDYawCmdPubl;
    ros::Publisher drone_yaw_to_look_publisher;

    ros::Subscriber drone_status_subs;
    ros::Subscriber drone_manager_status_subs;
    ros::Subscriber droneHLCommAckSub;
    ros::Subscriber drone_pos_reference_subs;
    ros::Subscriber drone_estimated_pose_subs;
    ros::Subscriber drone_position_reference_subscriber;
    ros::Subscriber drone_speed_reference_subscriber;
    ros::Subscriber tracking_object_sub;


public:
    DroneMissionPlannerEmulator();
    ~DroneMissionPlannerEmulator();
    void open(ros::NodeHandle & nIn);


    void sendCommandInPositionControlMode(double controller_step_command_x, double controller_step_command_y, double controller_step_command_z);
    void sendYawCommandInPositionControlMode(double controller_step_command_yaw);
    void sendCommandInSpeedControlMode(double vxfi, double vyfi);
    void sendCommandInRelTrajectoryControlMode(const std::vector<SimpleTrajectoryWaypoint> * const trajectory_waypoints_out, const int initial_checkpoint_out, const bool is_periodic_out);
    void sendCommandInAbsTrajectoryControlMode(droneMsgsROS::dronePositionRefCommand mission_point);
    void sendCommandInMovingManualAltitudMode(double cte_command_pitch, double cte_command_roll, double cte_command_height, double cte_command_yaw);
    void sendCommandInVisualServoingMode();
    void sendCommandForTakingoff();
    void sendCommandForLanding();
    void sendCommandForHovering();
    void sendCommandForLooping();
    void sendCommandForMovingInYaw();
    void clearCmd();

    void managerAckCallback(const droneMsgsROS::droneHLCommandAck::ConstPtr& msg);
    void droneCurrentPositionRefsSubCallback(const droneMsgsROS::dronePose::ConstPtr &msg);
    void droneCurrentSpeedsRefsSubCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg);
    void drone_estimated_GMR_pose_callback(const  droneMsgsROS::dronePose &msg);
    //void droneCurrentStatusSubCallback(const droneMsgsROS::droneStatus::ConstPtr &msg);
    void droneCurrentManagerStatusSubCallback(const droneMsgsROS::droneManagerStatus::ConstPtr &msg);
    void trackingObjectCallback(const std_msgs::Bool &msg);

    droneMsgsROS::droneManagerStatus getDroneManagerStatus(){return last_drone_manager_status_msg;}


};

#endif // DRONE_MISSION_PLANNER_EMULATOR_H

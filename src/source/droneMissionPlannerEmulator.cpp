#include "droneMissionPlannerEmulator.h"
using namespace std;

DroneMissionPlannerEmulator::DroneMissionPlannerEmulator()
{
    droneHLCommAckMsg.ack = false;
    tracker_is_tracking = false;
}

DroneMissionPlannerEmulator::~DroneMissionPlannerEmulator() 
{

}


//Calbacks for the Subscribers
void DroneMissionPlannerEmulator::managerAckCallback(const droneMsgsROS::droneHLCommandAck::ConstPtr& msg)
{
    cout<<"Mission Planner: Received"<<endl;
    droneHLCommAckMsg.time = msg->time;
    droneHLCommAckMsg.ack = msg->ack;

    if(droneHLCommAckMsg.ack)
        cout<<"Manager ack TRUE"<<endl;
    else if(!droneHLCommAckMsg.ack)
        cout<<"Manager ack FALSE"<<endl;

    return;
}

void DroneMissionPlannerEmulator::droneCurrentPositionRefsSubCallback(const droneMsgsROS::dronePose::ConstPtr &msg)
{
    current_drone_position_reference = (*msg);
}

void DroneMissionPlannerEmulator::droneCurrentSpeedsRefsSubCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg)
{
    current_drone_speed_reference = (*msg);
}

void DroneMissionPlannerEmulator::drone_estimated_GMR_pose_callback(const  droneMsgsROS::dronePose &msg)
{
    last_drone_estimated_GMRwrtGFF_pose  = (msg);
}


void DroneMissionPlannerEmulator::droneCurrentManagerStatusSubCallback(const droneMsgsROS::droneManagerStatus::ConstPtr &msg)
{
    last_drone_manager_status_msg = (*msg);

    switch(last_drone_manager_status_msg.status)
    {
        case droneMsgsROS::droneManagerStatus::HOVERING:
            cout<<"Manager Status: HOVERING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::HOVERING_VISUAL_SERVOING:
            cout<<"Manager Status: HOVERING VISUAL SERVOING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::LANDING:
            cout<<"Manager Status: LANDING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::LANDED:
            cout<<"Manager Status: LANDED"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::TAKINGOFF:
            cout<<"Manager Status: TAKINGOFF"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD:
            cout<<"Manager Status: MOVING MANUAL ALTITUD"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_POSITION:
            cout<<"Manager Status: MOVING POSITION"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_SPEED:
            cout<<"Manager Status: MOVING SPEED"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY:
            cout<<"Manager Status: MOVING TRAJECTORY"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_VISUAL_SERVOING:
            cout<<"Manager Status: MOVING VISUAL SERVOING"<<endl;
            break;
    }
}

void DroneMissionPlannerEmulator::trackingObjectCallback(const std_msgs::Bool &msg)
{
     tracker_is_tracking = msg.data;
     return;
}

void DroneMissionPlannerEmulator::open(ros::NodeHandle & nIn) 
{
    n = nIn;

    DroneModule::open(n);

    mission_planer_pub = n.advertise<droneMsgsROS::droneMissionPlannerCommand>("droneMissionPlannerCommand", 1, true);
    drone_position_reference_publisher = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("dronePositionRefs", 1);
    drone_speeds_reference_publisher     = n.advertise<droneMsgsROS::droneSpeeds>("droneSpeedsRefs", 1);
    drone_rel_trajectory_reference_publisher = n.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>("droneTrajectoryRefCommand", 1);
    drone_abs_trajectory_reference_publisher = n.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>("droneTrajectoryAbsRefCommand", 1);
    drone_yaw_reference_publisher = n.advertise<droneMsgsROS::droneYawRefCommand>("droneControllerYawRefCommand", 1);
    drone_yaw_to_look_publisher = n.advertise<droneMsgsROS::droneYawRefCommand>("droneYawToLook", 1);
    dronePitchRollCmdPubl = n.advertise<droneMsgsROS::dronePitchRollCmd>("command/pitch_roll",1, true);
    droneDAltitudeCmdPubl = n.advertise<droneMsgsROS::droneDAltitudeCmd>("command/dAltitude",1);
    droneDYawCmdPubl = n.advertise<droneMsgsROS::droneDYawCmd>("command/dYaw",1, true);
    dronePositionRefCommandPubl = n.advertise<droneMsgsROS::dronePositionRefCommand>("droneMissionPoint", 1, true);


    droneHLCommAckSub = n.subscribe("droneMissionHLCommandAck", 1, &DroneMissionPlannerEmulator::managerAckCallback,this);
    drone_pos_reference_subs = n.subscribe("trajectoryControllerPositionReferencesRebroadcast", 1, &DroneMissionPlannerEmulator::droneCurrentPositionRefsSubCallback,this);
    drone_estimated_pose_subs = n.subscribe("EstimatedPose_droneGMR_wrt_GFF", 1, &DroneMissionPlannerEmulator::drone_estimated_GMR_pose_callback,this);
    drone_position_reference_subscriber   = n.subscribe("trajectoryControllerPositionReferencesRebroadcast", 1, &DroneMissionPlannerEmulator::droneCurrentPositionRefsSubCallback, this);
    drone_speed_reference_subscriber      = n.subscribe("trajectoryControllerSpeedReferencesRebroadcast", 1, &DroneMissionPlannerEmulator::droneCurrentSpeedsRefsSubCallback, this);
    //drone_status_subs  = n.subscribe("droneStatusToMissionPlanner", 1, &DroneMissionPlannerEmulator::droneCurrentStatusSubCallback,this);
    drone_manager_status_subs  = n.subscribe("droneManagerStatus", 1, &DroneMissionPlannerEmulator::droneCurrentManagerStatusSubCallback,this);
    tracking_object_sub = n.subscribe("tracking_object",1,&DroneMissionPlannerEmulator::trackingObjectCallback, this);
}

void DroneMissionPlannerEmulator::sendCommandInPositionControlMode(double controller_step_command_x, double controller_step_command_y, double controller_step_command_z)
{
    cout<<"comand move() IN POSITION sent"<<endl;
    mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
    msg.mpCommand = mp_command;
    msg.drone_modules_names.clear();
    std::vector<std::string> modules_names;
    modules_names.push_back(MODULE_NAME_YAW_PLANNER);
    modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
    msg.drone_modules_names = modules_names;
    mission_planer_pub.publish(msg);

//    double current_xs, current_ys, current_zs, current_yaws;
//    current_xs = last_drone_estimated_GMRwrtGFF_pose.x;
//    current_ys = last_drone_estimated_GMRwrtGFF_pose.y;
//    current_zs = last_drone_estimated_GMRwrtGFF_pose.z;
//    current_yaws = last_drone_estimated_GMRwrtGFF_pose.yaw;

    if(controller_step_command_x !=0 || controller_step_command_y != 0 || controller_step_command_z != 0)
    {
        double current_xs, current_ys, current_zs;
        current_xs = current_drone_position_reference.x;
        current_ys = current_drone_position_reference.y;
        current_zs = current_drone_position_reference.z;


        drone_position_reference.header.stamp  = ros::Time::now();
        drone_position_reference.position_command.x = current_xs + controller_step_command_x;
        drone_position_reference.position_command.y = current_ys + controller_step_command_y;
        drone_position_reference.position_command.z = current_zs + controller_step_command_z;


        drone_position_reference_publisher.publish(drone_position_reference);
    }
}

void DroneMissionPlannerEmulator::sendYawCommandInPositionControlMode(double controller_step_command_yaw)
{
    cout<<"comand move() IN POSITION sent"<<endl;
    mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
    msg.mpCommand = mp_command;
    msg.drone_modules_names.clear();
    std::vector<std::string> modules_names;
    modules_names.push_back(MODULE_NAME_YAW_PLANNER);
    msg.drone_modules_names = modules_names;
    mission_planer_pub.publish(msg);


    double current_yaws = current_drone_position_reference.yaw;
    drone_yaw_reference.header.stamp = ros::Time::now();
    drone_yaw_reference.yaw = current_yaws + controller_step_command_yaw;

    drone_yaw_reference_publisher.publish(drone_yaw_reference);
}


void DroneMissionPlannerEmulator::sendCommandInSpeedControlMode(double vxfi, double vyfi)
{
    cout<<"comand move() IN SPEED sent"<<endl;
    mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_SPEED;
    msg.mpCommand = mp_command;
    msg.drone_modules_names.clear();
    mission_planer_pub.publish(msg);


//    drone_speed_reference.target_frame = "drone_GMR";
//    drone_speed_reference.reference_frame = "GFF";
//    drone_speed_reference.YPR_system = "wYvPuR";

    if(vxfi !=0 || vyfi != 0)
    {
        double current_vxfi, current_vyfi;
        current_vxfi = current_drone_speed_reference.dx;
        current_vyfi = current_drone_speed_reference.dy;

        if(vxfi == 0.0)
            vxfi = current_vxfi;
        if(vyfi == 0.0)
            vyfi = current_vyfi;

        drone_speed_reference.time = ros::Time::now().toSec();
        drone_speed_reference.dx = vxfi;
        drone_speed_reference.dy = vyfi;
        drone_speed_reference.dz     = 0.0;
        drone_speed_reference.dyaw   = 0.0;
        drone_speed_reference.dpitch = 0.0;
        drone_speed_reference.droll  = 0.0;

        drone_speeds_reference_publisher.publish(drone_speed_reference);
    }
}

void DroneMissionPlannerEmulator::sendCommandInRelTrajectoryControlMode(const std::vector<SimpleTrajectoryWaypoint> * const trajectory_waypoints_out, const int initial_checkpoint_out, const bool is_periodic_out)
{
    cout<<"comand move() IN TRAJECTORY sent"<<endl;
    droneHLCommAckMsg.ack = false;
    droneMsgsROS::dronePositionTrajectoryRefCommand drone_trajectory_reference_command;
    mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_TRAJECTORY;
    msg.mpCommand = mp_command;
    msg.drone_modules_names.clear();
    std::vector<std::string> modules_names;
    modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
    msg.drone_modules_names = modules_names;
    mission_planer_pub.publish(msg);
    ros::spinOnce();

    drone_trajectory_reference_command.header.stamp = ros::Time::now();
    for (std::vector<SimpleTrajectoryWaypoint>::const_iterator it = (*trajectory_waypoints_out).begin();
         it != (*trajectory_waypoints_out).end();
         ++it) {
        droneMsgsROS::dronePositionRefCommand next_waypoint;
        next_waypoint.x = it->x;
        next_waypoint.y = it->y;
        next_waypoint.z = it->z;
        drone_trajectory_reference_command.droneTrajectory.push_back(next_waypoint);
    }
    drone_trajectory_reference_command.initial_checkpoint = initial_checkpoint_out;
    drone_trajectory_reference_command.is_periodic = is_periodic_out;


    //ros::Duration(1).sleep();
    while(1)
    {
        cout<<"MP waiting ack.."<<endl;
        if(droneHLCommAckMsg.ack)
            break;

    }
    drone_rel_trajectory_reference_publisher.publish(drone_trajectory_reference_command);

}

void DroneMissionPlannerEmulator::sendCommandInAbsTrajectoryControlMode(droneMsgsROS::dronePositionRefCommand mission_point)
{
    cout<<"comand move() IN TRAJECTORY sent"<<endl;
    droneHLCommAckMsg.ack = false;
    mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_TRAJECTORY;
    msg.mpCommand = mp_command;
    msg.drone_modules_names.clear();
    std::vector<std::string> modules_names;
    modules_names.push_back(MODULE_NAME_TRAJECTORY_PLANNER);
    modules_names.push_back(MODULE_NAME_YAW_PLANNER);
    msg.drone_modules_names = modules_names;
    mission_planer_pub.publish(msg);
    ros::spinOnce();

    //ros::Duration(0.5).sleep();
    while(1)
    {
        cout<<"MP waiting ack.."<<endl;
        if(droneHLCommAckMsg.ack)
            break;

    }
    dronePositionRefCommandPubl.publish(mission_point);
}

void DroneMissionPlannerEmulator::sendCommandInMovingManualAltitudMode(double cte_command_pitch, double cte_command_roll, double cte_command_height, double cte_command_yaw)
{
    cout<<"Command move() MANUAL ALTITUD sent"<<endl;
    mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_MANUAL_ALTITUD;
    msg.mpCommand = mp_command;
    msg.drone_modules_names.clear();
    std::vector<std::string> modules_names;
    modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
    msg.drone_modules_names = modules_names;
    mission_planer_pub.publish(msg);

    if(cte_command_pitch != 0.0)
    {
        dronePitchRollCmdMsgs.pitchCmd = cte_command_pitch;
        dronePitchRollCmdPubl.publish(dronePitchRollCmdMsgs);
    }

    if(cte_command_roll != 0.0)
    {
        dronePitchRollCmdMsgs.rollCmd = cte_command_roll;
        dronePitchRollCmdPubl.publish(dronePitchRollCmdMsgs);
    }

    if(cte_command_height != 0.0)
    {
        droneDAltitudeCmdMsgs.dAltitudeCmd = cte_command_height;
        droneDAltitudeCmdPubl.publish(droneDAltitudeCmdMsgs);
    }
    if(cte_command_yaw != 0.0)
    {
        droneDYawCmdMsgs.dYawCmd = cte_command_yaw;
        droneDYawCmdPubl.publish(droneDYawCmdMsgs);
    }

}

void DroneMissionPlannerEmulator::sendCommandInVisualServoingMode()
{
    cout<<"comand move() IN VISUAL SERVOING sent"<<endl;
    droneHLCommAckMsg.ack = false;
    mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_VISUAL_SERVOING;
    msg.mpCommand = mp_command;
    msg.drone_modules_names.clear();
//    std::vector<std::string> modules_names;
//    modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
//    msg.drone_modules_names = modules_names;
    mission_planer_pub.publish(msg);
    ros::spinOnce();
}

void DroneMissionPlannerEmulator::sendCommandForTakingoff()
{
    cout<<"Command takeoff() sent"<<endl;
    clearCmd();
    msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::TAKE_OFF;
    msg.drone_modules_names.clear();
    std::vector<std::string> modules_names;
    modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
    msg.drone_modules_names = modules_names;
    mission_planer_pub.publish(msg);
}

void DroneMissionPlannerEmulator::sendCommandForHovering()
{
    cout<<"Command hover() sent"<<endl;
    clearCmd();
    mp_command = droneMsgsROS::droneMissionPlannerCommand::HOVER;
    msg.mpCommand = mp_command;
    msg.drone_modules_names.clear();
    std::vector<std::string> modules_names;
    modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
    msg.drone_modules_names = modules_names;
    mission_planer_pub.publish(msg);
}

void DroneMissionPlannerEmulator::sendCommandForLooping()
{
    cout<<"Command loop() sent"<<endl;
    clearCmd();
    mp_command = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_RIGHT;
    msg.mpCommand = mp_command;
    mission_planer_pub.publish(msg);
}

void DroneMissionPlannerEmulator::sendCommandForLanding()
{
    cout<<"Command land() sent"<<endl;
    clearCmd();
    mp_command = droneMsgsROS::droneMissionPlannerCommand::LAND;
    msg.mpCommand = mp_command;
    msg.drone_modules_names.clear();
    mission_planer_pub.publish(msg);
}


void DroneMissionPlannerEmulator::sendCommandForMovingInYaw()
{
    drone_yaw_reference.yaw = 1.5708;
    //drone_yaw_to_look_publisher.publish(drone_yaw_reference);
    drone_yaw_reference_publisher.publish(drone_yaw_reference);
}

void DroneMissionPlannerEmulator::clearCmd()
{
    dronePitchRollCmdMsgs.pitchCmd=0.0;
    dronePitchRollCmdMsgs.rollCmd=0.0;
    droneDAltitudeCmdMsgs.dAltitudeCmd=0.0;
    droneDYawCmdMsgs.dYawCmd=0.0;
}



